var gulp = require('gulp'),
    jade = require('gulp-jade');

gulp.task('jade', function() {
    return gulp.src('views/index.jade')
        .pipe(jade()) // pip to jade plugin
        .pipe(gulp.dest('')); // tell gulp our output folder
});

