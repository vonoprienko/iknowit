/**
 * Created by vasiliy on 21.04.16.
 */
var express = require('express'),
    Vocabulary = require('../models/data.model'),
    VerbsWordsData = require('../models/verbs.model');

var DataWords = {
    addword: function (req, res, next) {
        console.log(req.body);
        if (req.body.en !== "" && req.body.ua !== "") {
            Vocabulary.create(req.body, function (err, word) {
                if (err) return next(err);
                res.status(201).json({
                    success: true,
                    word: word
                })
            })
        }
    },

    getMyVocabularyWords: function (req, res, next) {
        var query = Vocabulary.aggregate([
            { $sample: { size: 10 } }
        ]);
        query.exec(function (err, words) {
            if (err) return next(err);
            res.status(200).json({
                success: true,
                words: words
            })
        });
    },

    getVocabulary: function (req, res, next) {
        Vocabulary.find({})
            .sort('-createdAt')
            .exec(function (err, vocabulary) {
            if (err) return next(err);
            //console.log(vocabulary);
            res.status(200).json({
                success: true,
                vocabulary: vocabulary
            })
        });
    },

    postIrregularWord: function (req, res, next) {
        //console.log(req.body);
        if (
            req.body.baseform != "" &&
            req.body.pastsimple  != "" &&
            req.body.pastparticiple  != "" &&
            req.body.translation  != "") {
            var verbs = new VerbsWordsData({
                baseForm: req.body.baseform,
                pastSimple: req.body.pastsimple,
                pastParticiple: req.body.pastparticiple,
                transcription: req.body.transcription,
                translate: req.body.translation
            });
            verbs.save(function (err, verb) {
                if (err) return next(err);
                res.json({
                    success: true,
                    verb: verb
                })
            });
        }
    },

    getVocabularyVerbs: function (req, res, next) {
        VerbsWordsData.find({})
            .exec(function (err, verbs) {
                if (err) return next(err);
                console.log(verbs);
                res.status(200).json({
                    success: true,
                    verbs: verbs
                })
            })
    },

    getWordTranslationTraining: function (req, res, next) {
        var query = Vocabulary.aggregate([
            { $sample: { size: 10 } }
        ]);
        query.exec(function (err, words) {
            if (err) return next(err);

            var wordsForReject = [];

            words.forEach(function (item) {
                    wordsForReject.push(item.ua); } );

            var wrong = Vocabulary.aggregate([
                { $match: { 'ua': { $ne: wordsForReject }  } },
                { $sample: { size: 40 } },
                { $project: { ua: 1, _id: 0 } }
            ]);
            wrong.exec(function (err, wrongWordsArray) {
                var fourWrongAnswer = [];
                var newWords = words.map(function (word, index) {
                    fourWrongAnswer = wrongWordsArray.slice(index * 4, index * 4 + 4);
                    word.wrongWords = fourWrongAnswer;
                    return word;
                });

                res.status(200).json({
                    success: true,
                    words: newWords
                })
            });

        });
    }

    //checkout: function (req, res, next) {
    //    var user = req.body.user,
    //        order = {
    //            paid: false,
    //            totalCost: req.body.data.totalCost,
    //            products: req.body.data.items,
    //            address: user.address,
    //            phone: user.phone,
    //            message: user.message
    //        };
    //
    //    if (user.id) {
    //        User.findOne({_id: user.id}, function (err, user) {
    //            if (err) return next(err);
    //            if (user) {
    //                order.user = user._id;
    //                Order.create(order, function (err, order) {
    //                    if (err) return next(err);
    //                    user.orders.push(order);
    //                    user.save(function (err) {
    //                        if (err) return next(err);
    //                    });
    //                    res.json({
    //                        success: true,
    //                        order: order
    //                    })
    //                });
    //            }
    //        });
    //    } else {
    //        order.guest = user.guest;
    //        Order.create(order, function (err, order) {
    //            if (err) return next(err);
    //            res.json({
    //                success: true,
    //                order: order
    //            })
    //        });
    //    }
    //},
    //
    //getAll: function (req, res, next) {
    //    Order.find().populate("user", "name avatar").exec(function (err, orders) {
    //        if (err) return next(err);
    //        res.json({
    //            success: true,
    //            orders: orders
    //        });
    //    })
    //},
    //
    //getOne: function(req, res, next) {
    //    var id = req.params.id;
    //    Order.findOne({_id: id}).populate("user", "name avatar").exec(function(err, order) {
    //        if (err) return next(err);
    //        if (order) {
    //            res.json({
    //                success: true,
    //                order: order
    //            });
    //        } else {
    //            return res.status(404).json({
    //                success: false,
    //                message: 'Order with id ' + id + ' can not be found'
    //            });
    //        }
    //    })
    //},
    //
    //update: function(req, res, next) {
    //    var id = req.params.id,
    //        body = {
    //            paid: req.body.paid,
    //            updatedAt: Date.now()
    //        };
    //
    //    Order.findByIdAndUpdate(id, {$set: body, $inc: {__v: 1}}, {
    //        runValidators: true,
    //        new: true
    //    }).populate("user", "name avatar").exec(function(err, order) {
    //        if (err) return next(err);
    //        if (order) {
    //            res.json({
    //                success: true,
    //                order: order
    //            });
    //        } else {
    //            return res.status(404).json({
    //                success: false,
    //                message: 'Order with id ' + id + ' can not be found'
    //            });
    //        }
    //    });
    //}
};

module.exports = DataWords;
