var express = require('express'),
    router = express.Router(),
    dataWords = require('./datawords'),
    upload = require('./upload'),
    multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty();

/*
 * Routes that can be accessed by any one
 */
router.post('/addword', dataWords.addword);
router.get('/getenwords', dataWords.getMyVocabularyWords);
router.get('/getvocabulary', dataWords.getVocabulary);

//irregular words
router.post('/postIrregularWord', dataWords.postIrregularWord);
router.get('/getvocabularyverbs', dataWords.getVocabularyVerbs);

//upload files
router.post('/uploadfile', upload.uploadFiles);

//training word-translate
router.get('/getwordtranslation', dataWords.getWordTranslationTraining);

module.exports = router;
