/**
 * Created by vasiliy on 21.04.16.
 */
var express = require('express'),
    fs = require('fs'),
    mkdirp = require('mkdirp');

var uploadFile = {

    uploadFiles: function (req, res, next) {
        var fsStream;
        console.log(req);
        console.log(__dirname);
        mkdirp(__dirname + '/../public/uploads/', function (err) {
            if (err) return res.status(500).send({message: 'Something is wrong... Problem accessing the server'});
            req.pipe(req.busboy);
            req.busboy.on('file', function (fieldname, file, filename) {
                var path = {
                    name: filename,
                    path: '/uploads/' + filename
                };
                fsStream = fs.createWriteStream('public/uploads/' + filename);
                file.pipe(fsStream);
                res.json(path);
            });
        });
    }
    
    //addword: function (req, res, next) {
    //    if (req.body.en !== "" && req.body.ua !== "") {
    //        Vocabulary.create(req.body, function (err, word) {
    //            if (err) return next(err);
    //            res.status(201).json({
    //                success: true,
    //                word: word
    //            })
    //        })
    //    }
    //},
    //
    //getMyVocabularyWords: function (req, res, next) {
    //    var query = Vocabulary.aggregate([
    //        { $sample: { size: 10 } }
    //    ]);
    //    query.exec(function (err, words) {
    //        if (err) return next(err);
    //        res.status(200).json({
    //            success: true,
    //            words: words
    //        })
    //    });
    //},
    //
    //getVocabulary: function (req, res, next) {
    //    Vocabulary.find({})
    //        .sort('-createdAt')
    //        .exec(function (err, vocabulary) {
    //        if (err) return next(err);
    //        //console.log(vocabulary);
    //        res.status(200).json({
    //            success: true,
    //            vocabulary: vocabulary
    //        })
    //    });
    //}
};

module.exports = uploadFile;
