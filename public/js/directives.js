/*myApp.directive('phoneInput', function ($filter, $browser) {
 return {
 require: 'ngModel',
 link: function ($scope, $element, $attrs, ngModelCtrl) {
 var listener = function () {
 var value = $element.val().replace(/[^0-9]/g, '');
 $element.val($filter('tel')(value, false));
 };

 // This runs when we update the text field
 ngModelCtrl.$parsers.push(function (viewValue) {
 return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
 });

 // This runs when the model gets updated on the scope directly and keeps our view in sync
 ngModelCtrl.$render = function () {
 $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
 };

 $element.bind('change', listener);
 $element.bind('keydown', function (event) {
 var key = event.keyCode;
 // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
 // This lets us support copy and paste too
 if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
 return;
 }
 $browser.defer(listener); // Have to do this or changes don't get picked up properly
 });

 $element.bind('paste cut', function () {
 $browser.defer(listener);
 });
 }
 };
 });

 myApp.directive('ngThumb', ['$window', function ($window) {
 var helper = {
 support: !!($window.FileReader && $window.CanvasRenderingContext2D),
 isFile: function (item) {
 return angular.isObject(item) && item instanceof $window.File;
 },
 isImage: function (file) {
 var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
 return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
 }
 };

 return {
 restrict: 'A',
 template: '<canvas/>',
 link: function (scope, element, attributes) {
 if (!helper.support) return;

 var params = scope.$eval(attributes.ngThumb);

 if (!helper.isFile(params.file)) return;
 if (!helper.isImage(params.file)) return;

 var canvas = element.find('canvas');
 var reader = new FileReader();

 reader.onload = onLoadFile;
 reader.readAsDataURL(params.file);

 function onLoadFile(event) {
 var img = new Image();
 img.onload = onLoadImage;
 img.src = event.target.result;
 }

 function onLoadImage() {
 var width = params.width || this.width / this.height * params.height;
 var height = params.height || this.height / this.width * params.width;
 canvas.attr({width: width, height: height});
 canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
 }
 }
 }
 }]);

 myApp.directive('modal', function () {
 return {
 templateUrl: "./partials/uploadPhotos.html",
 restrict: 'E',
 transclude: true,
 replace: true,
 scope: true,
 link: function postLink(scope, element, attrs) {
 scope.title = attrs.title;

 scope.$watch(attrs.visible, function (value) {
 if (value == true)
 $(element).modal('show');
 else
 $(element).modal('hide');
 });

 $(element).on('shown.bs.modal', function () {
 scope.$apply(function () {
 scope.$parent[attrs.visible] = true;
 });
 });

 $(element).on('hidden.bs.modal', function () {
 scope.$apply(function () {
 scope.$parent[attrs.visible] = false;
 });
 });
 }
 };
 });

 myApp.directive('toggleCheckbox', function() {

 return {
 restrict: 'A',
 transclude: true,
 replace: false,
 require: 'ngModel',
 link: function ($scope, $element, $attr, require) {

 var ngModel = require;

 // update model from Element
 var updateModelFromElement = function() {
 // If modified
 var checked = $element.prop('checked');
 if (checked != ngModel.$viewValue) {
 // Update ngModel
 ngModel.$setViewValue(checked);
 $scope.$apply();
 }
 };

 // Update input from Model
 var updateElementFromModel = function() {
 // Update button state to match model
 $element.trigger('change');
 };

 // Observe: Element changes affect Model
 $element.on('change', function() {
 updateModelFromElement();
 });

 // Observe: ngModel for changes
 $scope.$watch(function() {
 return ngModel.$viewValue;
 }, function() {
 updateElementFromModel();
 });

 // Initialise BootstrapToggle
 $element.bootstrapToggle();
 }
 };
 });*/

myApp.directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function (e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });