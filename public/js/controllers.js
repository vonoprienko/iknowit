myApp.controller("HomeCtrl", ['$scope',
  function($scope) {

  }
]);

myApp.controller("HeaderCtrl", ['$scope',
    function ($scope) {

    }
]);

myApp.controller("WordTranslateCtrl", ['$scope', 'WordsDataService', 'DataWords',
    function ($scope, WordsDataService, DataWords) {

        $scope.index = 0;
        //get words for training
        function getMy () {
            WordsDataService.getWordTranslation()
                .then(function (response) {
                    $scope.mywords = response.data.words;
                }, function (status) {
                    console.log(status);
                });
        };

        getMy();
        getExersize($scope.index);

        function getExersize (i) {

        }

    }
]);

myApp.controller("AuditionCtrl", ['$scope', 'WordsDataService', '$timeout',
    function ($scope, WordsDataService, $timeout) {

        $scope.userAnswer = "";
        $scope.index = 0;

        //get words for training
        function getMy () {
            WordsDataService.getMyWords()
                .then(function (response) {
                    $scope.mywords = response.data.words;
                    $scope.readbtn();
                }, function (status) {
                    console.log(status);
                });
        };

        getMy();

        //read the words
        $scope.readbtn = function () {
            responsiveVoice.speak($scope.mywords[$scope.index].en);
        };

        //button for next word
        $scope.nextWord = function () {
            if ($scope.index !== $scope.mywords.length - 1) {
                if ($scope.userAnswer == $scope.mywords[$scope.index].en) {
                    responsiveVoice.speak('Good job! Next word...');
                    $scope.index++;
                    $scope.userAnswer = "";
                    $scope.showCorrect = "That's right, keep well";
                    $('#labelMessage').show();
                    $timeout(function hideMessage() {
                        $('#labelMessage').hide(500);
                    }, 3000);

                    $scope.readbtn();
                } else {
                    responsiveVoice.speak('Wrong!');
                    $scope.showCorrect = "Wrong, try again";
                    $('#labelMessage').show();
                    $timeout(function hideMessage() {
                        $('#labelMessage').hide(500);
                    }, 3000);
                }
            } else {
                $scope.index++;
                $scope.showCorrect = "Good work! You practice all the words.";
                responsiveVoice.speak("Good work! You practice all the words.");
                $('#labelMessage').show();
            }
        };

        $scope.skipWord = function () {
            if ($scope.index !== $scope.mywords.length - 1) {
                $scope.index++;
                $scope.readbtn();
            } else {
                $scope.index++;
                $scope.showCorrect = "Good work! You practice all the words.";
                responsiveVoice.speak("Good work! You practice all the words.");
                $('#labelMessage').show();
            }
        };

        //click nextword by "Press Enter button"
        $("#enterWord").keyup(function(event){
            if(event.keyCode == 13){
                $("#nextButton").click();
            }
        });
    }
]);

myApp.controller("VocabularyCtrl", ['$scope', 'WordsDataService', 'DataWords', 'Upload', '$timeout',
    function ($scope, WordsDataService, DataWords, Upload, $timeout) {
        $scope.wordsArr = [];

        //get vocabularies at load page
        getVocabularyWords();
        getVocabularyIrregular();

        //add simple word to vocabulary
        $scope.addWord = function () {
            if ($scope.entext != "" && $scope.uatext) {
                DataWords.addWord($scope.entext, $scope.uatext, $scope.filepath)
                    .then(function (response) {
                        $scope.data = response.data.word;
                        getVocabularyWords();
                    }, function (status) {
                        console.log(status);
                    });
            } else {
                alert('Pls enter words in fields');
            }
            $scope.entext = "";
            $scope.uatext = "";
        };

        //add irregular verbs to vocabulary
        $scope.addIrregular = function () {
            if ($scope.baseform != "" && $scope.pastsimple != "" && $scope.pastparticiple != "" && $scope.translation != "") {
                DataWords.addIrregularWord($scope.baseform, $scope.pastsimple, $scope.pastparticiple, $scope.transcription, $scope.translation)
                    .then(function (response) {
                        console.log(response.data);
                        $scope.data = response.data.verb;
                        getVocabularyIrregular();
                        $scope.baseform = "";
                        $scope.pastsimple = "";
                        $scope.pastparticiple = "";
                        $scope.transcription = "";
                        $scope.translation = "";
                    })
            } else {
                alert('Pls enter words in fields');
            }
        };

        $scope.read = function (verb) {
            WordsDataService.read(verb.baseForm + ', ' + verb.pastParticiple + ', ' + verb.pastSimple);
        };

        function getVocabularyWords () {
            WordsDataService.getVocabulary()
                .then(function (response) {
                    $scope.vocabulary = response.data.vocabulary;
                }, function (status) {
                    console.log(status);
                });
        }

        function getVocabularyIrregular () {
            WordsDataService.getVocabularyVerbs()
                .then(function (response) {
                    $scope.irregularVerbs = response.data.verbs;
                }, function (status) {
                    console.log(status);
                });
        }

        //====== upload file
        $scope.uploadPic = function(file) {
            file.upload = Upload.upload({
                url: 'api/v1/uploadfile',
                file: file
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                    $scope.filepath = file.result.path;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }

        //tabs click change
        $('.nav-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    }
]);

myApp.controller("LoginCtrl", ['$scope', 'UserAuthFactory',
    function ($scope, UserAuthFactory) {
        $scope.login = function () {
            var name = $scope.user.name,
                password = $scope.user.password;
            console.log(name);
            console.log(password);
            if (name !== undefined && password !== undefined) {
                UserAuthFactory.login(name, password).success(function(data) {
                    console.log('============ data');
                    console.log(data);

                    $location.path("#/");
                }).error(function(status) {
                    alert('Oops something went wrong!');
                });
            } else {
                alert('Invalid credentials');
            }
        }
    }
]);

