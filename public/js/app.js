var myApp = angular.module('ngclient',
        [
            'ngRoute',
            'ngFileUpload'
        ]);

myApp.constant('config', {
  apiUrl: '/api/v1',
  appVersion: 1.0
});

myApp.run(function($rootScope, $templateCache) {
  $rootScope.$on('$viewContentLoaded', function() {
    $templateCache.removeAll();
  });
});

myApp.config(function($routeProvider) {
  $routeProvider
      .when('/', {
        templateUrl: 'partials/home.html',
        controller: 'LoginCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/addwords', {
        templateUrl: 'partials/addwords.html',
        controller: 'AddWordsCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/audition', {
        templateUrl: 'partials/audition.html',
        controller: 'AuditionCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/vocabulary', {
        templateUrl: 'partials/vocabulary.html',
        controller: 'VocabularyCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/training/word-translate', {
        templateUrl: 'partials/wordTranslate.html',
        controller: 'WordTranslateCtrl',
        access: {
          requiredLogin: false
        }
      }).otherwise({
        redirectTo: '/'
      });
});