
myApp.service('WordsDataService', function($http, config) {
    var urlBase = config.apiUrl;

    this.getVocabulary = function() {
        return $http.get(config.apiUrl + '/getvocabulary');
    };

    this.getMyWords = function() {
        return $http.get(config.apiUrl + '/getenwords');
    };

    this.getWordTranslation = function() {
        return $http.get(config.apiUrl + '/getwordtranslation');
    };

    this.getVocabularyVerbs = function () {
        return $http.get(config.apiUrl + '/getvocabularyverbs');
    };

    this.read = function (words) {
        responsiveVoice.speak(words, "US English Male", {rate: 0.5});
    };

});