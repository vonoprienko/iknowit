/*
myApp.factory('UserWishlistFactory', function ($http, config) {

    var _wishlistFactory = {};

    _wishlistFactory.getWishlist = function (userId) {
        return $http.get(config.apiUrl + '/products/wishlist/' + userId);
    };

    _wishlistFactory.deleteOneWishlist = function (id, userId) {
        return $http.delete(config.apiUrl + '/products/wishlist/' + userId + '/' + id);
    };

    return _wishlistFactory;
});*/

myApp.factory('DataWords', function ($http, config) {
   var _dataWords = {};

    _dataWords.addIrregularWord = function (baseform, pastsimple, pastparticiple, transcription, translation) {
        var data = JSON.stringify({
            baseform: baseform,
            pastsimple: pastsimple,
            pastparticiple: pastparticiple,
            transcription: transcription,
            translation: translation

        });
        return $http.post(config.apiUrl + '/postIrregularWord', data);
    };

    _dataWords.addWord = function(en, ua, filepath) {
        var data = JSON.stringify({
            en: en,
            ua: ua,
            filepath: filepath
        });
        return $http.post(config.apiUrl + '/addword', data);
    };

    return _dataWords;
});
