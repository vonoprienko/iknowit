/**
 * Created by vasiliy on 21.04.16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// create a product schema
var WordsDataSchema = new Schema({
    en: {type: String, required: true},
    ua: {type: String, required: true},
    transcription: {type: String},
    filepath: { type: String },
    createdAt: {type: Date, default: Date.now}
});

function timeStamp(next) {
    var now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
}

WordsDataSchema.pre('save', timeStamp);

// add a text index to the name & description fields
WordsDataSchema.index({
    name: 'text',
    description: 'text'
});

module.exports = mongoose.model('WordsData', WordsDataSchema);