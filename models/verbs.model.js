/**
 * Created by vasiliy on 25.04.16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// create a product schema
var VerbsSchema = new Schema({
    baseForm: {type: String, unique : true, required: true},
    pastSimple: {type: String, unique : true, required: true},
    pastParticiple: {type: String, unique : true, required: true},
    transcription: {type: String},
    translate: {type: String, required: true}

});

function timeStamp(next) {
    var now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
}

VerbsSchema.pre('save', timeStamp);

// add a text index to the name & description fields
VerbsSchema.index({
    name: 'text',
    description: 'text'
});

module.exports = mongoose.model('VerbsWordsData', VerbsSchema);