module.exports.config = {
    getEnv: function () {
        return {
            mailgun: {
                key: 'key-cc5f4eb3ba23938ff5126ed828bb325f',
                domain: 'sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                login: 'postmaster@sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                password: '1q2w3e'
            }
        }
    }
};

var passport = require('passport'),
    jwt = require('jsonwebtoken'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcrypt-nodejs'),
    Users = require('../../app/_global/models/user.model'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    mailgun = require('mailgun-js')({apiKey: exports.config.getEnv().mailgun.key, domain: exports.config.getEnv().mailgun.domain});


/**
 * Encrypt password through "bcrypt" module
 * @param password {string} - password for saving into db
 * @returns {*} {string} - encrypted by "bcrypt"
 */
exports.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/**
 * Validate password through "bcrypt" module
 * @param passwordInput {string} - password from input
 * @param passwordUser {string} - encrypted password from database
 * @returns {*} {boolean} - if passwords the same "true"
 */
exports.validPassword = function (passwordInput, passwordUser) {
    return bcrypt.compareSync(passwordInput, passwordUser);
};

/**
 * Emails delivery function
 * @param to {string} - email of recipient
 * @param title {string} - email title
 * @param content {string} - email text or html
 */
exports.sendEmail = function (to, title, content) {
    if (to) {
        var data = {
            from: 'Papirux <noreply@pascalium.com>',
            to: to,
            subject: title,
            text: content
        };
        mailgun.messages().send(data, function (error, body) {
            if (error) next(error);
            console.log(body);
        });
    }
};

exports.passportStrategies = function (passport) {

    passport.serializeUser(function (user, callback) {
        callback(null, user.id);
    });

    passport.deserializeUser(function (id, callback) {
        Users.findById(id, function (err, user) {
            callback(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, callback) {
        process.nextTick(function () {
            Users.findOne({$or: [{'email': email}, {'uid': req.body.uid.toUpperCase()}]}, function (err, user) {
                if (err) {
                    return callback(err);
                }
                if (user) {
                    return callback(null, false,
                        req.signMessage = (user.email == email) ? 'That email is already taken.' : 'That UID is already taken.');
                }
                var newUser = new Users({
                    displayName: req.body.displayName,
                    uid: req.body.uid,
                    email: email,
                    salt: Math.random().toString(36).slice(-5),
                    password: exports.generateHash(password),
                    subdomain: exports.getSubdomain(req, subLevel) ? exports.getSubdomain(req, subLevel) : ''
                });
                newUser.save(function (err) {
                    if (err) {
                        req.signMessage = err.errors;
                        return callback(err);
                    }
                    var content = "Registration complete! \n Your credentials are UID: " + newUser.uid + " password: " + password + " ",
                        to = newUser.email,
                        title = "Thanks for registration";
                    exports.sendEmail(to, title, content);
                    return callback(null, newUser);
                });
            });
        });
    }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, callback) {
            process.nextTick(function () {
                Users.findOne({$or: [{'email': email}, {'uid': email.toUpperCase()}]}, function (err, user) {
                    if (err) {
                        return callback(err);
                    }
                    if (!user) {
                        return callback(null, false, req.loginMessage = 'No user found.');
                    }
                    if (!exports.validPassword(password, user.password)) {
                        return callback(null, false, req.loginMessage = 'Wrong password.');
                    }
                    if (user.subdomain !== exports.getSubdomain(req, subLevel)) {
                        return callback(null, false, req.loginMessage = 'You try login at unauthorized subdomain.');
                    }
                    return callback(null, user);
                });
            });
        }));
};

exports.checkUser = function (req, res, next) {
    if (req.headers.authorization) {
        jwt.verify(req.headers.authorization.split(' ')[1], exports.config.tokenSalt, function (err, data) {
            if (err) return res.status(500).json({message: err});
            if (data) {
                Users
                    .findOne({_id: data.id, salt: data.salt})
                    .exec(function (err, data) {
                        if (err) return res.status(500).json({message: err});

                        if (data) {
                            req.user = data;
                            next();
                        } else {
                            res.status(401).json({message: 'User Not Found or you try login at unauthorized subdomain'});
                        }
                    });
            } else {
                res.status(401).json({message: 'Authorization Token Not Valid'});
            }
        });
    } else {
        res.status(401).json({message: 'Authorization Token Not Valid'});
    }

};