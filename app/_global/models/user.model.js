var mongoose = require('mongoose'),
    moment = require('moment');

/**
 * Return date plus 3 months from now
 * @returns {*}
 */
var monthsFromNow = function(){
    return moment().add(3, 'm');
};

var userSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    password: {
        type: String
    },
    salt: {
        type: String
    },
    passwordReset: {
        type: String
    },
    picture: {
        type: String,
        default: 'http://www.gravatar.com/avatar/0.jpg'
    },
    displayName: {
        type: String
    },
    dateCreated: {
        type: Date,
        default: Date.now
    },
    socials: {
        facebook: {
            type: String
        },
        twitter: {
            type: String
        },
        google: {
            type: String
        },
        linkedin: {
            type: String
        }
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    updatedAt: {
        type: Date
    }
});

module.exports = mongoose.model('User', userSchema);