module.exports = function (app, router) {

    var AuthController = require('./auth.controller');

    router.route('/auth/signup')
        .post(AuthController.signUp);

    router.route('/auth/login')
        .post(AuthController.logIn);

    router.route('/auth/twitter')
        .post(AuthController.twitter);

    router.route('/auth/google')
        .post(AuthController.google);

    router.route('/auth/facebook')
        .post(AuthController.facebook);

    router.route('/auth/linkedin')
        .post(AuthController.linkedin);

    router.route('/auth/restore/forgot')
        .post(AuthController.restoreForgot);

    router.route('/auth/restore/reset')
        .post(AuthController.restoreReset);
};
