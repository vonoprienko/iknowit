myApp.factory('UserAuthFactory', function($window, $location, $http, config) {
    return {
        login: function(name, password) {
            return $http.post(config.apiUrl + '/auth/login', {
                name: name,
                password: password
            });
        },
        register: function(name, password, confirmpassword, usermail) {
            return $http.post(config.apiUrl + '/register', {
                name: name,
                password: password,
                confirmpassword: confirmpassword,
                usermail: usermail
            });
        },
        logout: function() {
            if (AuthenticationFactory.isLogged) {
                AuthenticationFactory.isLogged = false;
                delete AuthenticationFactory.user;
                delete AuthenticationFactory.admin;
                delete AuthenticationFactory.userId;
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.user;
                delete $window.sessionStorage.admin;
                delete $window.sessionStorage.userId;
                delete $window.sessionStorage.phone;
                $location.path("#/");
            }
        }
    }
});