var async = require('async'),
    request = require('request'),
    jwt = require('jsonwebtoken'),
    twitterAPI = require('node-twitter-api'),
    passport = require('passport'),
    NodeCache = require("node-cache"),
    subLevel = 0;

var User = require('../../_global/models/user.model');

var settings = require('../../_global/config'),
    generateHash = settings.generateHash,
    sendEmail = settings.sendEmail,
    passportStrategies = settings.passportStrategies(passport),
    cache = new NodeCache({stdTTL: 600, checkperiod: 0});

/**
 * Logic for signUp and logIn
 * @param err
 * @param res
 * @param req
 * @param next
 * @param user
 * @returns {*}
 */
function passportAuthLogic(err, res, req, next, user) {
    if (err) {
        return next(err);
    }
    if (!user) {
        return res.status(401).send({message: req.signMessage});
    }
    req.logIn(user, function (err) {
        if (err) {
            return next(err);
        }
        var payload = {
            id: user._id,
            salt: user.salt
        };
        var token = jwt.sign(payload, settings.config.tokenSalt);
        return res.send({user: user, token: token, message: unFreezeNotification});
    });
}

/**
 * Method for /auth/signup route
 * use "passport" module and "passportStrategies"
 * to verify user
 * @param req
 * @param res
 * @param next
 */

exports.signUp = function (req, res, next) {
    passport.authenticate('local-signup', function (err, user, info) {
        passportAuthLogic(err, res, req, next, user);
    })(req, res, next);
};

exports.logIn = function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        passportAuthLogic(err, res, req, next, user);
        unFreeze (user);
    })(req, res, next);
};

exports.twitter = function (req, res) {
    var twitter = new twitterAPI({
        consumerKey: settings.config.getEnv().keys.twitter.consumerKey,
        consumerSecret: settings.config.getEnv().keys.twitter.consumerSecret,
        callback: req.body.redirectUri
    });

    if (!req.body.oauth_token || !req.body.oauth_verifier) {
        twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
            if (error) {
                console.log("Error getting OAuth request token : " + error.data);
                return res.status(500).send({message: 'Twitter error'});
            }
            var data = {
                oauth_token: requestToken,
                oauth_token_secret: requestTokenSecret
            };

            cache.set(requestToken, data);

            res.json(data);
        });
    } else {
        async.waterfall([
            function (callback) {
                cache.get(req.body.oauth_token, function (err, value) {
                    callback(err, value);
                });
            }, function (token, callback) {
                twitter.getAccessToken(token.oauth_token, token.oauth_token_secret, req.body.oauth_verifier, function (err, accessToken, accessTokenSecret, results) {
                    if (err) {
                        return res.status(500).send({message: err.message});
                    }
                    var params = {
                        'include_entities': true,
                        'skip_status': true,
                        'include_email': true
                    };
                    twitter.verifyCredentials(accessToken, accessTokenSecret, params, function (err, profile, response) {
                        if (err) {
                            return res.status(500).send({message: err.message});
                        }
                        callback(err, profile);
                    });
                });
            }, function (profile, callback) {
                //remove fields "id and email"
                console.log(profile);
                var user = {
                    picture: profile.profile_image_url_https,
                    displayName: profile.name,
                    socials: {
                        twitter: profile.id_str
                    }
                };

                console.log('-------TWITTER-------');
                console.log(profile);
                console.log(user);

                loginCheck(req, res, user, 'twitter', function (data) {
                    callback(false, data);
                });

            }, function (user, callback) {
                setToken(user, function (data) {
                    callback(user, data);
                })
            }
        ], function (user, token) {
            if (user.account.termination.isFrozen) {
                var unFreezeNotification = 'Your user unfrozen';
            }
            unFreeze (user);
            res.json({user: user, token: token, message: unFreezeNotification});
        });
    }
};

exports.google = function (req, res) {
    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
    console.log(settings.config.getEnv().keys.google.clientID);
    console.log(settings.config.getEnv().keys.google.clientSecret);
    console.log(req.body.redirectUri);
    var params = {
        code: req.body.code,
        client_id: settings.config.getEnv().keys.google.clientID,
        client_secret: settings.config.getEnv().keys.google.clientSecret,
        redirect_uri: req.body.redirectUri,
        grant_type: 'authorization_code'
    };

    async.waterfall([
        function (callback) {
            request.post(accessTokenUrl, {json: true, form: params}, function (err, response, token) {
                var accessToken = token.access_token;
                var headers = {Authorization: 'Bearer ' + accessToken};

                request.get({url: peopleApiUrl, headers: headers, json: true}, function (err, response, profile) {
                    if (profile.error) {
                        return res.status(500).send({message: profile.error.message});
                    }
                    callback(err, profile);
                });
            });
        }, function (profile, callback) {
            var user = {
                email: profile.email,
                picture: profile.picture.replace('sz=50', 'sz=200'),
                displayName: profile.name,
                socials: {
                    google: profile.sub
                }
            };
            console.log('-------GOOGLE-------');
            console.log(user);

            loginCheck(req, res, user, 'google', function (data) {
                callback(false, data);
            });

        }, function (user, callback) {
            setToken(user, function (data) {
                callback(user, data);
            })
        }
    ], function (user, token) {
        if (user.account.termination.isFrozen) {
            var unFreezeNotification = 'Your user unfrozen';
        }
        unFreeze (user);
        res.json({user: user, token: token, message: unFreezeNotification});
    });
};

exports.facebook = function (req, res) {
    var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'],
        accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token',
        graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');

    var params = {
        code: req.body.code,
        client_id: req.body.clientId,
        client_secret: settings.config.getEnv().keys.fb,
        redirect_uri: req.body.redirectUri
    };

    async.waterfall([
        function (callback) {
            request.get({url: accessTokenUrl, qs: params, json: true}, function (err, response, accessToken) {
                if (response.statusCode !== 200) {
                    return res.status(500).send({message: accessToken.error.message});
                }

                request.get({url: graphApiUrl, qs: accessToken, json: true}, function (err, response, profile) {
                    if (response.statusCode !== 200) {
                        return res.status(500).send({message: profile.error.message});
                    }
                    callback(err, profile);
                });
            });
        }, function (profile, callback) {
            var user = {
                email: profile.email,
                picture: 'https://graph.facebook.com/' + profile.id + '/picture?type=large',
                displayName: profile.name,
                socials: {
                    facebook: profile.id
                }
            };

            loginCheck(req, res, user, 'facebook', function (data) {
                callback(false, data);
            });

        }, function (user, callback) {
            setToken(user, function (data) {
                callback(user, data);
            })
        }
    ], function (user, token) {
        if (user.account.termination.isFrozen) {
            var unFreezeNotification = 'Your user unfrozen';
        }
        unFreeze (user);
        res.json({user: user, token: token, message: unFreezeNotification});
    });
};

exports.linkedin = function (req, res) {
    var accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
    var peopleApiUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)';
    console.log(settings.config.getEnv().keys.linkedin.clientID);
    console.log(settings.config.getEnv().keys.linkedin.clientSecret);
    console.log(req.body.redirectUri);
    var params = {
        code: req.body.code,
        client_id: settings.config.getEnv().keys.linkedin.clientID,
        client_secret: settings.config.getEnv().keys.linkedin.clientSecret,
        redirect_uri: req.body.redirectUri,
        grant_type: 'authorization_code'
    };

    async.waterfall([
        function (callback) {
            request.post(accessTokenUrl, {form: params, json: true}, function (err, response, body) {
                if (response.statusCode !== 200) {
                    return res.status(response.statusCode).send({message: body.error_description});
                }
                var params = {
                    oauth2_access_token: body.access_token,
                    format: 'json'
                };

                request.get({url: peopleApiUrl, qs: params, json: true}, function (err, response, profile) {
                    callback(err, profile);
                });
            });
        },
        function (profile, callback) {
            var user = {
                email: profile.emailAddress,
                picture: profile.pictureUrl,
                displayName: profile.firstName + ' ' + profile.lastName,
                socials: {
                    linkedin: profile.id
                }
            };

            loginCheck(req, res, user, 'linkedin', function (data) {
                callback(false, data);
            });
        }, function (user, callback) {
            setToken(user, function (data) {
                callback(user, data);
            })
        }
    ], function (user, token) {
        if (user.account.termination.isFrozen) {
            var unFreezeNotification = 'Your user unfrozen';
        }
        unFreeze (user);
        res.json({user: user, token: token, message: unFreezeNotification});
    });
};

exports.restoreForgot = function (req, res) {
    var value = req.body.val;

    User
        .findOne({$or: [ { "email": value }, {"uid": value.toUpperCase()} ] })
        .exec(function (err, user) {
            if (err) {
                return res.status(500).send({message: err});
            }
            if (user) {
                if (user.email) {
                    user.passwordReset = Math.random().toString(36).slice(-8);
                    user.save(function () {
                        var content = 'Code for reseting your password: ' + user.passwordReset,
                            to = user.email,
                            title = 'Reset email';
                        sendEmail(to, title, content);
                        res.json({message: 'Email with additional information sent'});
                    });
                } else {
                    res.status(400).send({message: 'This account do not has email'});
                }
            } else {
                res.status(400).send({message: 'User not found'});
            }
        });
};

exports.restoreReset = function (req, res) {
    var resetCode = req.body.restoreCode;

    User.findOne({passwordReset: resetCode}, function (err, user) {
        if (err) {
            return res.status(500).send({message: err});
        }

        if (user) {
            user.password = generateHash(req.body.newPass);
            user.save(function (err, doc) {
                res.status(200).send({message: 'New password was successfully saved'});
            });
        } else {
            res.status(403).send({message: 'Wrong restore code!'});
        }
    });
};

function loginCheck(req, res, user, options, callback) {
    var network = [];
    if (user.email) {
        network.push({
            email: user.email
        });
    }
    var account = {
        socials: {}
    };
    account.socials[options] = user.socials[options];
    network.push(account);
    User.findOne({
        $or: network
    }, function (err, data) {
        var high = 1000, low = 0;
        if (err) {
            return res.status(500).send({message: err});
        }
        if (data) {
            data.isSocialFirst = false;
            data.save();
            if (data.subdomain !== settings.getSubdomain(req, subLevel)) {
                return res.status(500).send({message: 'User Not Found or you try login at unauthorized subdomain'});
            }
            if (data.socials[options] === user.socials[options]) {
                callback(data);
            } else {
                var userAccount = data.socials,
                    text;
                if (userAccount.facebook) {
                    text = 'Facebook';
                } else if (userAccount.google) {
                    text = 'Google Account';
                } else if (userAccount.twitter) {
                    text = 'Twitter';
                } else if (userAccount.linkedin) {
                    text = 'LinkedIn';
                } else {
                    text = 'Custom';
                }
                return res.status(500).send({message: 'You have signed up with ' + text + ' account previously which uses the same e-mail address. Please try to login with ' + text + ' account again.'});
            }
        } else {
            var newUser = new User(user),
                passForMail = Math.random().toString(36).slice(-8);
            newUser.uid = 'user' + global.settings.userNumber;
            global.settings.userNumber++;
            newUser.password = generateHash(passForMail);
            newUser.salt = Math.random().toString(36).slice(-5);
            newUser.isSocialFirst = true;
            newUser.subdomain = settings.getSubdomain(req, subLevel);
            newUser.save(function (err) {
                if (err) {
                    return res.status(500).send({message: err.message});
                }
                global.settings.save();
                var content = "Registration complete! \n Your credentials are UID: " + newUser.uid + " password: " + passForMail + " ",
                    to = newUser.email,
                    title = "Thanks for registration";
                sendEmail(to, title, content);
                callback(newUser);
            });

        }
    });
}

function setToken(user, callback) {
    var payload = {
        id: user._id,
        salt: user.salt
    };
    callback(jwt.sign(payload, settings.config.tokenSalt));
}

function unFreeze (user) {

    if (typeof user._id != 'undefined') {
        User.findOne({_id: user._id}, function (err, user) {
            user.account.termination.isFrozen = false;
            user.account.termination.dateOfFreeze = undefined;
            user.save();
        });
    }
}



